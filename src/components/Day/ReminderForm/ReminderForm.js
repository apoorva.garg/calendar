import React from 'react';
import TimePicker from 'react-time-picker';
import moment from 'moment';
import './ReminderForm.scss';

const reminderForm = props => {
  const time = props.reminder.time
    ? moment(props.reminder.time, "HH:mm a")
    : moment()
        .hour(0)
        .minute(0);

  return (
    <form
      method="post"
      onSubmit={e => props.handleCreateUpdateReminder(e,  props.reminder)}
    >
      <textarea
        className="description"
        placeholder="Reminder"
        maxLength="30"
        defaultValue={props.reminder.description}
      />

      <TimePicker
        showSecond={false}
        defaultValue={time}
        format="h:mm a"
        use12Hours
        inputReadOnly/>
        
      <button className="btn-submit">Submit</button>

      <button
        className="btn-cancel"
        onClick={() => props.handleSetEditDay(null)}
      >
        Cancel
      </button>
    </form>
  );
};

export default reminderForm;
