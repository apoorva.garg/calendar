import React from 'react';
import  { Link } from 'react-router-dom';
import './HeaderMonth.scss';

const headerMonth = (props) => {

    const name = props.currMonth.name;
    return(
        <header className="month-header">
        <div className= "row">
            <Link to={"/" + props.prevMonth.count}>
            <i className = "fas fa-arrow-left"/>
            </Link>
        </div>
        <div className= "row">
            <h1>{name}</h1>
        </div>
        <div className= "row">
            <Link to={"/" + props.nextMonth.count}>
            <i className = "fas fa-arrow-right"/>
            </Link>
        </div>
    </header>
    );
}


export default headerMonth;