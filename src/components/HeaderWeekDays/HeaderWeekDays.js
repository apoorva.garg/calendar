import React from 'react';
import './HeaderWeekDays.scss';

const headerWeekDays = (props) => (
    <header className="weekdays">
        {props.days.map((day, i) => (
            <strong key={i}>{day}</strong>
        ))}
    </header>
);

export default headerWeekDays;