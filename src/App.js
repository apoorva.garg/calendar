import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Calendar from './containers/Calendar/Calendar';
import Aux from './hoc/Auxi/Auxi';

class App extends Component {
  render(){
    return(
        <Aux>
            <Switch>
              <Route to="/:year/:month" component={Calendar} />
              <Route to="/" exact component={Calendar} />
            </Switch>
        </Aux>
    );
  }
}

export default App;
