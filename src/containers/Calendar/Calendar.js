import React, { Component } from 'react';
import './Calendar.scss';
import moment from 'moment';

import HeaderMonth from '../../components/HeaderMonth/HeaderMonth';
import HeaderWeekDays from '../../components/HeaderWeekDays/HeaderWeekDays';
import Day from '../../components/Day/Day';

class Calendar extends Component {

    state = {
        currMonth: {},
        nextMonth: {},
        prevMonth: {}
    };

    componentDidMount() {
        this.createMonth(this.props);
    }

    createMonth(props) {
        const currMonth = props.match.params.year && props.match.params.month ? `${props.match.params.year}`-`${props.match.params.month}` : moment().format("YYYY-MM");
        const nextMonth = moment(currMonth).add(1, "M").format("YYYY-MM");
        const prevMonth = moment(currMonth).subtract(1, "M").format("YYYY-MM");
        console.log(currMonth);
        this.setState({
            currMonth: {
                date: currMonth,
                name: moment(currMonth).format("MMMM YYYY"),
                days: moment(currMonth).daysInMonth(),
                editDay: null
            },
            nextMonth: {
                date: nextMonth,
                count: nextMonth.replace("-",'/'),
                name: moment(nextMonth).format("MMMM YYYY")
            }, 
            prevMonth: {
                date: prevMonth,
                count: prevMonth.replace("-",'/')
            }
        });
    }

    handleSetEditDay = day => {
        this.setState({
            currMonth: {
                ...this.state.currMonth,
                editDay: day
            }
        });
    };

    constructDays() {
            const days = [];
            const props = {
                editDay: this.state.currMonth.editDay,
                handleSetEditDay: this.handleSetEditDay
            }
            for (let i = 1; i <= this.state.currMonth.days; i++) {
                let date = `${this.state.currMonth.date}-${("0" + i).slice(-2)}`;
                props["date"] = date;
                props["day"] = i;
                console.log(props);
                if (i === 1) {
                  props["firstDayIndex"] = moment(date)
                    .startOf("month")
                    .format("d");
                } else {
                  delete props["firstDayIndex"];
                }
                days.push(<Day key={i} {...props}/>);
              }
              console.log(days);
              return days;
    }

    render() {

        const weekdays = moment.weekdays();
        const days = this.constructDays();

        return(
            <div className="calendar">
                <HeaderMonth 
                    currMonth={this.state.currMonth}
                    nextMonth={this.state.nextMonth}
                    prevMonth={this.state.prevMonth}
                />
                <HeaderWeekDays days={weekdays} />
                <section className="days">{days}</section>
            </div>
        );
    }
}

export default Calendar;